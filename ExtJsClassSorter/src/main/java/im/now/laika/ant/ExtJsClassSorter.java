package im.now.laika.ant;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.tools.ant.BuildLogger;
import org.apache.tools.ant.DefaultLogger;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Target;
import org.apache.tools.ant.types.Resource;
import org.apache.tools.ant.types.ResourceCollection;
import org.apache.tools.ant.types.resources.BaseResourceCollectionWrapper;
import org.apache.tools.ant.types.resources.FileResource;

public class ExtJsClassSorter extends BaseResourceCollectionWrapper {
    private String[] externs = new String[0];
  
    public void setExtern(String extern) {
        if (!extern.trim().isEmpty()) {
            externs = extern.split(",");
        }
    }
    
    @Override
    protected synchronized Collection<Resource> getCollection() {        
        List<Resource> resources = new LinkedList();
        
        Map<String,Resource> nameToRes = new HashMap();
        String rootName = null;
        
        Project p = new Project();        
        Path basePath = getProject().getBaseDir().toPath();
            
        ResourceCollection rc = getResourceCollection();
        Iterator<Resource> iter = rc.iterator();
        while (iter.hasNext()) {
            FileResource resource = iter.next().as(FileResource.class);
            Path file = resource.getFile().toPath();
            
            // File text.
            String text = readFile(file);
            // Class name. Null if not ExtJS class.
            String name = parseName(text);
            // List of required ExtJS classes.
            List<String> requires = parseRequires(text, externs);
            
            if (name == null) {
                System.out.printf("%s does not appear to be an ExtJS class.\n", 
                        basePath.relativize(file));
                resources.add(resource);
                continue;
            }
            
            // Save resource by class name.
            nameToRes.put(name, resource);
            if (rootName == null) {
                rootName = name;
            }
            
            // Add class and deps to project.
            Target t = new Target();
            t.setName(name);
            for (String require : requires) {
                t.addDependency(require);
            }
            p.addTarget(t);            
            
            System.out.printf("%s requires %s\n", name, requires);
        }
               
        if(p.getTargets().size() != 0) {
            
            Vector<Target> sortedTargets = p.topoSort(rootName, p.getTargets());
            
            for (Target t : sortedTargets) {            
            resources.add(nameToRes.get(t.getName()));
            }
        }       
               
        return resources;
    }
    
    private boolean startsWith(String s, String[] items) {
        for (String item : items) {
            if (s.startsWith(item)) {
                return true;
            }
        }
        return false;
    }
        
    private String parseName(String text) {        
        final Pattern NAME_REGEX = Pattern.compile("Ext\\.define.*?[\"'](?<name>[A-z.]*)[\"']");
        Matcher m = NAME_REGEX.matcher(text);
        if (m.find()) {
            return m.group("name");
        } else {
            return null;
        }
    }
    
    private List<String> parseRequires(String text, String[] predefs) {
        final Pattern REQUIRES_REGEX = Pattern.compile("requires.*?:.*?\\[(?<body>.*?)\\]");
        Matcher m = REQUIRES_REGEX.matcher(text);
        List<String> requires = new LinkedList();
        if (m.find()) {
            String body = m.group("body");
            if (body == null) {
                body = "";
            }
            for (String name : body.split("[,\"'\\s]")) {
                name = name.trim();
                if (name.isEmpty() || startsWith(name, predefs)) {
                    continue;
                }                
                requires.add(name);
            }
        }
        return requires;
    }
    
    private String readFile(Path file) {
        try {
            List<String> lines = Files.readAllLines(file, Charset.defaultCharset());
            StringBuilder sb = new StringBuilder();
            for (String line : lines) {
                sb.append(line.trim());
            }
            return sb.toString();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

}
