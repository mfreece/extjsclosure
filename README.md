ExtJS + Google Closure Compiler
================================

Using Closure to build ExtJS apps is tricky because Ext has its own modules
system. This project adds a simple Ant task that uses regex to parse dependencies
from the ExtJS ```requires: [ ... ]``` attribute, and sorts the file list so that
classes are always included after their dependencies.

See example project ```build.xml``` file for more details.